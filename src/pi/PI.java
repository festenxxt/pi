package pi;

import java.util.Random;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class PI extends Application {

    static String m[][] = new String[10][10];
    static Button b[][] = new Button[10][10];

    /*Preciso saber para criar minha matriz com barcos:
        Nome
        Tamanho
        Se o barco na posição X não estrapola o limite do mapa
        Se não tem nenhum barco no meio
        Somente após isso colocar o barco no mapa (Matriz)
     */
    static void mostrarMatriz() {
        System.out.print("");
        for (int l = 0; l < 10; l++) {
            for (int c = 0; c < 10; c++) {
                System.out.print(m[l][c] + "  ");
            }
            System.out.println("");
        }

    }

    static void criarMatriz() {
        Random random = new Random();
        for (int l = 0; l < 10; l++) { //linha
            for (int c = 0; c < 10; c++) { //coluna
                m[l][c] = "W";
            }

        }

    }

    static void subMarino() {
        Random random = new Random();
        int s = 0;
        do {
            int linha = random.nextInt(9);
            int coluna = random.nextInt(9);
            m[linha][coluna] = "S";
            s++;
        } while (s <= 4);
    }

    static void destroyer() {
        Random random = new Random();
        int d = 0;
        do {
            //int linha = random.nextInt(9);
            //int coluna = random.nextInt(9);
            int linha = random.nextInt(9);
            int coluna = random.nextInt(9);
            if (validaPosicao(linha, coluna, 2)) {

                if (m[linha][coluna].equals("W") && linha % 2 == 0 && m[linha][coluna + 1].equals("W")) {
                    m[linha][coluna] = "D";
                    m[linha][coluna + 1] = "D";
                    d++;
                } else if (m[linha][coluna].equals("W") && linha % 2 != 0 && m[linha + 1][coluna].equals("W")) {
                    m[linha][coluna] = "D";
                    m[linha + 1][coluna] = "D";
                    d++;
                }
            }

        } while (d <= 3);

    }

    static void cruzados() {
        Random random = new Random();
        int d = 0;
        do {
            //int linha = random.nextInt(9);
            //int coluna = random.nextInt(9);
            int linha = random.nextInt(9);
            int coluna = random.nextInt(9);
            if (validaPosicao(linha, coluna, 3)) {

                if (m[linha][coluna].equals("W") && linha % 2 == 0 && m[linha][coluna + 1].equals("W") && m[linha][coluna + 2].equals("W")) {
                    m[linha][coluna] = "C";
                    m[linha][coluna + 1] = "C";
                    m[linha][coluna + 2] = "C";
                    d++;
                }
                if (m[linha][coluna].equals("W") && linha % 2 != 0 && m[linha + 1][coluna].equals("W") && m[linha + 2][coluna].equals("W")) {
                    m[linha][coluna] = "C";
                    m[linha + 1][coluna] = "C";
                    m[linha + 2][coluna] = "C";
                    d++;
                }
            }
        } while (d <= 2);

    }

    static void navio() {
        Random random = new Random();
        int d = 0;
        do {
            //int linha = random.nextInt(9);
            //int coluna = random.nextInt(9);
            int linha = random.nextInt(9);
            int coluna = random.nextInt(9);
            if (validaPosicao(linha, coluna, 4)) {

                if (m[linha][coluna].equals("W") && linha % 2 == 0 && m[linha][coluna + 1].equals("W")
                        && m[linha][coluna + 2].equals("W") && m[linha][coluna + 3].equals("W")) {
                    m[linha][coluna] = "N";
                    m[linha][coluna + 1] = "N";
                    m[linha][coluna + 2] = "N";
                    m[linha][coluna + 3] = "N";
                    d++;
                }
                if (m[linha][coluna].equals("W") && linha % 2 != 0 && m[linha + 1][coluna].equals("W")
                        && m[linha + 2][coluna].equals("W") && m[linha + 3][coluna].equals("W")) {
                    m[linha][coluna] = "N";
                    m[linha + 1][coluna] = "N";
                    m[linha + 2][coluna] = "N";
                    m[linha + 3][coluna] = "N";
                    d++;
                }
            }
        } while (d <= 1);

    }

    static void portaAviao() {
        Random random = new Random();
        int d = 0;
        do {
            //int linha = random.nextInt(9);
            //int coluna = random.nextInt(9);
            int linha = random.nextInt(9);
            int coluna = random.nextInt(9);
            if (validaPosicao(linha, coluna, 5)) {

                if (m[linha][coluna].equals("W") && linha % 2 == 0 && m[linha][coluna + 1].equals("W")
                        && m[linha][coluna + 2].equals("W") && m[linha][coluna + 3].equals("W")
                        && m[linha][coluna + 4].equals("W")) {
                    m[linha][coluna] = "P";
                    m[linha][coluna + 1] = "P";
                    m[linha][coluna + 2] = "P";
                    m[linha][coluna + 3] = "P";
                    m[linha][coluna + 4] = "P";
                    d++;
                }
                if (m[linha][coluna].equals("W") && linha % 2 != 0 && m[linha + 1][coluna].equals("W")
                        && m[linha + 2][coluna].equals("W") && m[linha + 3][coluna].equals("W")
                        && m[linha + 4][coluna].equals("W")) {
                    m[linha][coluna] = "P";
                    m[linha + 1][coluna] = "P";
                    m[linha + 2][coluna] = "P";
                    m[linha + 3][coluna] = "P";
                    m[linha + 4][coluna] = "P";
                    d++;
                }
            }
        } while (d <= 0);

    }

    static boolean validaPosicao(int linha, int coluna, int tamanho) {

        if (linha + tamanho <= 9 && coluna + tamanho <= 9) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

        criarMatriz();
        subMarino();
        destroyer();
        cruzados();
        navio();
        portaAviao();
        mostrarMatriz();
        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception //Palco
    {
        GridPane grid = new GridPane(); //Layout quem organiza as crianças
        Scene scene = new Scene(grid, 1280, 700); //Cena quem mostra as crianças
        primaryStage.setScene(scene); //Montar cena no Palco
        primaryStage.show(); //Abrir cortina

        for (int l = 0; l < 10; l++) {
            for (int c = 0; c < 10; c++) {
                b[l][c] = new Button("Atira em mim");
                final int L = l, C = c; //Condição para desarme
                grid.getChildren().add(b[l][c]); //pega as criança e adiciona elas
                GridPane.setRowIndex(b[l][c], l); //Montar as linhas da matriz de botões
                GridPane.setColumnIndex(b[l][c], c); //Montar as linhas da matriz de botões
                switch (m[l][c]) {
                    case ("W"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Errouuuuuu");
                                b[L][C].setDisable(true);
                                b[L][C].setText("W");

                            }
                        });
                        break;
                    case ("S"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Acertou o Submarino");
                                b[L][C].setDisable(true);
                                b[L][C].setText("S");
                            }
                        });
                        break;
                    case ("D"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Acertou o Destroyer");
                                b[L][C].setDisable(true);
                                b[L][C].setText("D");
                            }
                        });
                        break;
                    case ("C"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Acertou o Cruzador");
                                b[L][C].setDisable(true);
                                b[L][C].setText("C");
                            }
                        });
                        break;
                    case ("N"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showInternalMessageDialog(null, "Acerto o Navio");
                                b[L][C].setDisable(true);
                                b[L][C].setText("N");
                            }
                        });
                        break;
                    case ("P"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showInternalMessageDialog(null, "Acertou a Porta");
                                b[L][C].setDisable(true);
                                b[L][C].setText("P");
                            }
                        });
                        break;

                }

            }
        }
    
        Stage secondStage = new Stage();
        GridPane gird2 = new GridPane(); //Layout quem organiza as crianças
        Scene scene2 = new Scene(gird2, 1280, 700); //Cena quem mostra as crianças
        secondStage.setScene(scene2); //Montar cena no Palco
        secondStage.show(); //Abrir cortina

        for (int l = 0; l < 10; l++) {
            for (int c = 0; c < 10; c++) {
                b[l][c] = new Button("Atira em mim");
                final int L = l, C = c; //Condição para desarme
                gird2.getChildren().add(b[l][c]); //pega as criança e adiciona elas
                GridPane.setRowIndex(b[l][c], l); //Montar as linhas da matriz de botões
                GridPane.setColumnIndex(b[l][c], c); //Montar as linhas da matriz de botões
                switch (m[l][c]) {
                    case ("W"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Errouuuuuu");
                                b[L][C].setDisable(true);
                                b[L][C].setText("W");

                            }
                        });
                        break;
                    case ("S"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Acertou o Submarino");
                                b[L][C].setDisable(true);
                                b[L][C].setText("S");
                            }
                        });
                        break;
                    case ("D"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Acertou o Destroyer");
                                b[L][C].setDisable(true);
                                b[L][C].setText("D");
                            }
                        });
                        break;
                    case ("C"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showMessageDialog(null, "Acertou o Cruzador");
                                b[L][C].setDisable(true);
                                b[L][C].setText("C");
                            }
                        });
                        break;
                    case ("N"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showInternalMessageDialog(null, "Acerto o Navio");
                                b[L][C].setDisable(true);
                                b[L][C].setText("N");
                            }
                        });
                        break;
                    case ("P"):
                        //Bloco de ação !
                        b[l][c].setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JOptionPane.showInternalMessageDialog(null, "Acertou a Porta");
                                b[L][C].setDisable(true);
                                b[L][C].setText("P");
                            }
                        });
                        break;

                }

            }
        }
    }

}
